import itertools
import os
import pickle

import numpy as np
import xlrd


class Ayat:
    def __init__(self):
        self.ayat = ""
        self.topic = []

    def __getitem__(self, item):
        return getattr(self, item)

    def parse(self, ayat, topic):
        self.ayat = ayat
        self.topic = topic


def get_data(data_path=None):
    if data_path is None:
        data_path = os.path.dirname(os.path.realpath(__file__)) + "/dataset.xls"
    book = xlrd.open_workbook('dataset_master.xls')
    sh = book.sheet_by_index(0)
    ayat = Ayat()
    for row in range(1, sh.nrows, 1):
        ayat.parse(sh.cell_value(row, 0), sh.cell_value(row, 1))
        yield ayat


def get_batch(doc_iter, size):
    data = [(doc['ayat'], doc['topic'])
            for doc in itertools.islice(doc_iter, size)
            if doc['topic']]

    if not len(data):
        return np.asarray([], dtype=int), np.asarray([], dtype=int).tolist()

    x_text, y = zip(*data)

    x_text = list(x_text)
    y = list(y)

    # to_remove = []
    # doc_num = 0
    #
    # for article in x_text:
    #     if article.isSpace() or (article == ""):
    #         to_remove.append(doc_num)
    #
    #     doc_num += 1
    #
    # to_remove.reverse()
    # for i in to_remove:
    #     del x_text[i]
    #     del y[i]

    return x_text, y


def iter_minibatches(doc_iter, minibatch_size):
    x_text, y = get_batch(doc_iter, minibatch_size)
    while len(x_text):
        yield x_text, y
        x_text, y = get_batch(doc_iter, minibatch_size)


data_stream = get_data()

x_train_raw, y_train_raw = get_batch(data_stream, 40)
x_tes_raw, y_tes_raw = get_batch(data_stream, 20)

print("Train set is %d documents" % (len(y_train_raw)))
print("Test set is %d documents" % (len(y_tes_raw)))

# Dump the dataset to a pickle file.
pickle.dump(
    (x_train_raw, y_train_raw, x_tes_raw, y_tes_raw),
    open("data/raw_text_dataset.pickle", "wb")
)
